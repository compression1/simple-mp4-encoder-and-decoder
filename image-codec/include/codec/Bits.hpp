#pragma once

#ifndef BITS_H
#define BITS_H

#include <cstdint>
#include <filesystem>
#include <istream>
#include <memory>
#include <ostream>

class BitsWriter {
    std::unique_ptr<std::ostream> stream;
    char buffer;
    std::size_t buffer_position;

  public:
    BitsWriter(std::unique_ptr<std::ostream> &&stream);
    BitsWriter(std::filesystem::path const &file);
    ~BitsWriter();

    void write_bool(bool bit);
    void write_uint32(std::uint32_t value, std::size_t bit_count);
    void write_int32(std::int32_t value, std::size_t bit_count);
    void flush();

    std::ostream const *get_stream() const;
};

class BitsReader {
    std::unique_ptr<std::istream> stream;
    char buffer;
    std::size_t buffer_position;

  public:
    BitsReader(std::unique_ptr<std::istream> &&stream);
    BitsReader(std::filesystem::path const &file);

    bool read_bool();
    std::uint32_t read_uint32(std::size_t bit_count);
    std::int32_t read_int32(std::size_t bit_count);

    bool data_left() const;
};

std::size_t bits_required_unsigned(std::uint32_t value);
std::size_t bits_required_signed(std::int32_t value);

#endif // BITS
