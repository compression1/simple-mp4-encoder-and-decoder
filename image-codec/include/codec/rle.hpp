#pragma once

#ifndef RLE_H
#define RLE_H

#include "Bits.hpp"
#include <cstdint>
#include <vector>

namespace codec {
    struct RlePoint {
        std::uint8_t padding;
        int value;

        RlePoint(std::uint8_t padding, int value);

        void encode(BitsWriter &writer, std::size_t bits_for_value) const;
        static RlePoint decode(BitsReader &reader, std::size_t bits_for_value);
    };

    void runlength_encode(std::vector<int> const &data, BitsWriter &writer);
    std::vector<int> runlength_decode(BitsReader &reader, std::size_t const amount);
} // namespace codec

#endif // RLE_H
