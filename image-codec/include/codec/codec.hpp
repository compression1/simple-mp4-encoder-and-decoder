#pragma once

#ifndef CODEC_H
#define CODEC_H

#include "Bits.hpp"
#include "ImageConfig.hpp"
#include <filesystem>
#include <iostream>
#include <optional>
#include <unordered_map>
#include <vector>

namespace codec {
    typedef std::int16_t coefficient_t;
    typedef std::vector<std::pair<std::uint8_t, coefficient_t>> rle_components;
    typedef std::vector<coefficient_t> dc_components_t;

    std::size_t const BLOCK_SIZE = 4;
    std::size_t const MATRIX_SIZE = BLOCK_SIZE * BLOCK_SIZE;

    template <typename T>
    std::vector<T> image_to_blocks(
        std::vector<T> const &image,
        std::size_t const block_width,
        std::size_t const block_height,
        std::size_t const image_width) {
        std::size_t const image_height = image.size() / image_width;

        if (image_width * image_height != image.size())
            throw std::invalid_argument("The image is not a rectangle.");
        if (image_width % block_width != 0)
            throw std::invalid_argument(
                "The image width is not a perfect fit for the block width.");
        if (image_height % block_height != 0)
            throw std::invalid_argument(
                "The image height is not a perfect fit for the block height.");

        std::size_t const blockrows_per_image = image_height / block_height;
        std::size_t const blocks_per_blockrow = image_width / block_width;
        std::size_t const elements_per_blockrow = image_width * block_height;

        std::vector<T> rearranged;
        rearranged.reserve(image.size());

        // don't ask
        for (std::size_t blockrow = 0; blockrow < blockrows_per_image; blockrow += 1)
            for (std::size_t block = 0; block < blocks_per_blockrow; block += 1)
                for (std::size_t row = 0; row < block_height; row += 1)
                    for (std::size_t col = 0; col < block_width; col += 1) {
                        std::size_t const index = blockrow * elements_per_blockrow //
                                                  + row * image_width              //
                                                  + block * block_width            //
                                                  + col;
                        rearranged.push_back(image[index]);
                    }
        return rearranged;
    }

    template <typename T>
    std::vector<T> blocks_to_image(
        std::vector<T> const &image,
        std::size_t const block_width,
        std::size_t const block_height,
        std::size_t const image_width) {
        std::size_t const image_height = image.size() / image_width;

        return image_to_blocks(
            image, block_width, image_width / block_width, block_width * block_height);
    }

    template <typename T>
    std::vector<T> zigzag_inner(std::vector<T> &&image, std::vector<std::size_t> const &indices) {
        if (image.size() % indices.size() != 0)
            throw std::invalid_argument("zig-zag blocks don't fit in image");

        std::vector<T> cache;
        cache.reserve(indices.size());
        for (std::size_t offset = 0; offset < image.size(); offset += indices.size()) {
            cache.clear();

            for (std::size_t index = 0; index < indices.size(); index += 1)
                cache.push_back(image[offset + indices[index]]);
            for (std::size_t index = 0; index < indices.size(); index += 1)
                image[offset + index] = cache[index];
        }

        return image;
    }

    template <typename T> std::vector<T> zigzag(std::vector<T> &&image) {
        std::vector<std::size_t> const ZIGZAG_INDICES{
            0, 1, 4, 8, 5, 2, 3, 6, 9, 12, 13, 10, 7, 11, 14, 15};
        return zigzag_inner(std::move(image), ZIGZAG_INDICES);
    }

    template <typename T> std::vector<T> undo_zigzag(std::vector<T> &&image) {
        std::vector<std::size_t> const ZIGZAG_REVERSE_INDICES{
            0, 1, 5, 6, 2, 4, 7, 12, 3, 8, 11, 13, 9, 10, 14, 15};

        return zigzag_inner(std::move(image), ZIGZAG_REVERSE_INDICES);
    }

    /**
     * Encodes a given image, supplied in RAW format, with its metadata.
     */
    void encode(
        BitsWriter &writer,
        std::vector<char> const &raw_image,
        ImageConfig const &config,
        std::optional<std::ofstream> &logfile);

    void encode_dct_image(BitsWriter &writer, std::vector<int> &&image, bool use_rle);
    std::vector<int> decode_dct_image(BitsReader &reader, ImageConfig const &config);

    std::vector<int> forward_dct(
        std::vector<int> const &image,
        std::vector<int> const &quantisation_factors,
        std::size_t width,
        std::optional<std::ofstream> &logfile);

    std::vector<int> reverse_dct(
        std::vector<int> &&image,
        std::vector<int> const &quantisation_factors,
        std::size_t width);
    /**
     * Applies tiling, dct and quantisation, and zig-zag encoding on a given image, supplied in RAW
     * format.
     */
    void encode_image(
        BitsWriter &writer,
        std::vector<char> const &image,
        ImageConfig const &config,
        std::optional<std::ofstream> &logfile);

    void encode_image(
        BitsWriter &writer,
        std::vector<int> const &image,
        ImageConfig const &config,
        std::optional<std::ofstream> &logfile);

    /**
     * Decodes a given image, supplied in encoded format.
     * @return A decoded image.
     */
    std::vector<char> const decode(BitsReader &reader);

    /**
     * Decompresses a given image by applying reverse zig-zag, dct and quantisation, and tiling.
     */
    std::vector<int> decode_image(BitsReader &reader, ImageConfig const &config);

    /**
     * Performs DCT and quantisation on an image.
     * @param image The image to transform.
     * @param factors The quantisation factors.
     * @return The image with DCT and quantisation performed.
     */
    std::vector<int> dct_and_quantise(
        std::vector<int> &&image,
        std::vector<int> const &factors,
        std::optional<std::ofstream> &logfile);

    std::vector<int>
    undo_dct_and_quantise(std::vector<int> &&image, std::vector<int> const &factors);
} // namespace codec
#endif // CODEC_H
