#pragma once

#ifndef IMAGECONFIG_H
#define IMAGECONFIG_H

#include "Bits.hpp"
#include <vector>

namespace codec {

    /**
     * Settings used for image compression.
     */
    struct ImageConfig {
        std::vector<int> quantisation_factors;
        std::size_t width;
        std::size_t height;
        bool use_rle;

        ImageConfig(
            std::vector<int> &&quantisation_factors,
            std::size_t width,
            std::size_t height,
            bool use_rle);

        void encode(BitsWriter &writer) const;
        static ImageConfig decode(BitsReader &reader);

        bool operator==(ImageConfig const &that) const;
    };
} // namespace codec

#endif // IMAGECONFIG_H
