#pragma once

#ifndef UTIL_H
#define UTIL_H

#include "codec/codec.hpp"
#include <filesystem>
#include <fstream>
#include <vector>

namespace codec {
    int const RLE_USED_SIZE = sizeof(bool);
    int const QUANT_MATRIX_SIZE = 16 * sizeof(int);
    int const IMAGE_WIDTH_SIZE = sizeof(size_t);
    int const META_DATA_SIZE = RLE_USED_SIZE + QUANT_MATRIX_SIZE + IMAGE_WIDTH_SIZE;

    std::vector<int> matrix_from_file(std::filesystem::path const &filepath);
    std::vector<char> read_file(std::filesystem::path const &path);

    std::vector<char> ints_to_chars(std::vector<int> const &input);
    std::vector<int> chars_to_ints(std::vector<char> const &input);

    template <typename T>
    void
    log_block(std::vector<T> const &image, std::ofstream &logfile, std::size_t block_offset = 0) {
        logfile << std::string("BLOCK NR.") << std::to_string(block_offset) << std::endl;

        for (size_t i = 0; i < BLOCK_SIZE; i++) {
            for (size_t j = 0; j < BLOCK_SIZE; j++) {
                logfile << std::to_string(image[block_offset + i * BLOCK_SIZE + j])
                        << std::string(", ");
            }
            logfile << std::endl;
        }
    }
} // namespace codec

#endif
