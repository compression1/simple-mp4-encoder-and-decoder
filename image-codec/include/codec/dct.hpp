#pragma once

#ifndef DCT_HPP
#define DCT_HPP

#include <optional>
#include <vector>

namespace codec {
    std::vector<float> dct(std::vector<float> &&image, std::optional<std::ofstream> &logfile);
    std::vector<float> reverse_dct(std::vector<float> &&image);
} // namespace codec

#endif // DCT_HPP
