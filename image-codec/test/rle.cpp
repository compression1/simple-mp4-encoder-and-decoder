#include "codec/rle.hpp"
#include "codec/Bits.hpp"
#include <catch2/catch.hpp>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>

void check_rle(std::vector<int> original) {
    BitsWriter writer(std::make_unique<std::stringstream>());
    codec::runlength_encode(original, writer);
    writer.flush();

    std::string data = ((std::stringstream *)(writer.get_stream()))->str();
    BitsReader reader(std::make_unique<std::stringstream>(data));
    CHECK(original == codec::runlength_decode(reader, original.size()));
}

TEST_CASE("RLE encoding works", "[codec][rle]") {
    check_rle({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
    check_rle({16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
    check_rle({-8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8});
    check_rle({8, 7, 6, 5, 4, 3, 2, 1, -1, -2, -3, -4, -5, -6, -7, -8});
    check_rle({
        1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 1, //
        16, 15, 14, 13, 12, 11, 10, 9,  8,  7,  6,  5,  4,  3,  2,  1, //
        -8, -7, -6, -5, -4, -3, -2, -1, 1,  2,  3,  4,  5,  6,  7,  8, //
        8,  7,  6,  5,  4,  3,  2,  1,  -1, -2, -3, -4, -5, -6, -7, -8 //
    });
}

TEST_CASE("RLE encoding works with zeros", "[codec][rle][zero]") {
    check_rle(std::vector<int>(16));
    check_rle(std::vector<int>(16 * 17));
    check_rle({1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2});
    check_rle({1, 0, 3, 0, 5, 0, 7, 0, 9, 0, 11, 0, 13, 0, 15, 0});
    check_rle({1, 0, 3, 0, 5, 0, 0, 0, 9, 14, 11, 0, 13, 0, 15, 0});
    check_rle({
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0,  0,  0, 0,  0, 0,  2, //
        1, 0, 3, 0, 5, 0, 7, 0, 9, 0,  11, 0, 13, 0, 15, 0, //
        1, 0, 3, 0, 5, 0, 0, 0, 9, 14, 11, 0, 13, 0, 15, 0  //
    });
}
