#include <catch2/catch.hpp>
#include <codec/codec.hpp>
#include <codec/dct.hpp>
#include <codec/quantisation.hpp>
#include <fstream>

TEST_CASE("images can be divided into blocks", "[codec][codec-block]") {
    {
        std::vector<int> const not_blocked{0,  1,  2,  3,  4,  5,  //
                                           6,  7,  8,  9,  10, 11, //
                                           12, 13, 14, 15, 16, 17, //
                                           18, 19, 20, 21, 22, 23};

        {
            std::vector<int> const blocked{0,  1,  6,  7,  2,  3,  //
                                           8,  9,  4,  5,  10, 11, //
                                           12, 13, 18, 19, 14, 15, //
                                           20, 21, 16, 17, 22, 23};
            CHECK(codec::image_to_blocks(not_blocked, 2, 2, 6) == blocked);
        }

        {
            std::vector<int> const blocked{0,  1,  2,  6,  7,  8,  //
                                           3,  4,  5,  9,  10, 11, //
                                           12, 13, 14, 18, 19, 20, //
                                           15, 16, 17, 21, 22, 23};
            CHECK(codec::image_to_blocks(not_blocked, 3, 2, 6) == blocked);
        }

        {
            std::vector<int> const blocked{0,  6,  12, 18, 1,  7,  //
                                           13, 19, 2,  8,  14, 20, //
                                           3,  9,  15, 21, 4,  10, //
                                           16, 22, 5,  11, 17, 23};
            CHECK(codec::image_to_blocks(not_blocked, 1, 4, 6) == blocked);
        }

        {
            std::vector<int> const blocked{0, 1, 6,  7,  12, 13, 18, 19, //
                                           2, 3, 8,  9,  14, 15, 20, 21, //
                                           4, 5, 10, 11, 16, 17, 22, 23};
            CHECK(codec::image_to_blocks(not_blocked, 2, 4, 6) == blocked);
        }
    }
    {
        std::vector<int> const not_blocked{0,  1,  6,  7,  //
                                           2,  3,  8,  9,  //
                                           4,  5,  10, 11, //
                                           12, 13, 18, 19, //
                                           14, 15, 20, 21, //
                                           16, 17, 22, 23};

        {
            std::vector<int> const blocked{0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11,
                                           12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
            CHECK(codec::image_to_blocks(not_blocked, 2, 3, 4) == blocked);
        }
    }
}

TEST_CASE("blocked images can be unblocked", "[codec][codec-unblock]") {
    std::vector<int> const not_blocked{0,  1,  2,  3,  4,  5,  //
                                       6,  7,  8,  9,  10, 11, //
                                       12, 13, 14, 15, 16, 17, //
                                       18, 19, 20, 21, 22, 23};

    {
        std::vector<int> const blocked{0,  1,  6,  7,  2,  3,  //
                                       8,  9,  4,  5,  10, 11, //
                                       12, 13, 18, 19, 14, 15, //
                                       20, 21, 16, 17, 22, 23};
        CHECK(codec::blocks_to_image(blocked, 2, 2, 6) == not_blocked);
    }

    {
        std::vector<int> const blocked{0,  1,  2,  6,  7,  8,  //
                                       3,  4,  5,  9,  10, 11, //
                                       12, 13, 14, 18, 19, 20, //
                                       15, 16, 17, 21, 22, 23};
        CHECK(codec::blocks_to_image(blocked, 3, 2, 6) == not_blocked);
    }

    {
        std::vector<int> const blocked{0, 6,  12, 18, //
                                       1, 7,  13, 19, //
                                       2, 8,  14, 20, //
                                       3, 9,  15, 21, //
                                       4, 10, 16, 22, //
                                       5, 11, 17, 23};
        CHECK(codec::blocks_to_image(blocked, 1, 4, 6) == not_blocked);
    }
}

TEST_CASE(
    "an unblocked blocked image is equal to the original image",
    "[codec][codec-block-unblock]") {
    auto image = GENERATE(chunk(128 * 128, take(128 * 128 * 16, random(-1000000, 1000000))));

    auto bw = GENERATE(2, 4, 8);
    auto bh = GENERATE(2, 4, 8);
    auto iw = GENERATE(8, 64, 128, 256);

    CHECK(codec::blocks_to_image(codec::image_to_blocks(image, bw, bh, iw), bw, bh, iw) == image);
}

TEST_CASE("DCT is reversable", "[codec][dct]") {
    std::vector<float> data{
        705895, 980862, 753055, 295227, 614997, 841329, 371861, 682900, 812758, 213533, 996996,
        39831,  490932, 445663, 829540, 475149, 368315, 285675, 607717, 285411, 792544, 69746,
        481606, 557928, 21301,  571891, 325378, 341718, 318603, 366719, 377482, 704708, 250760,
        588283, 625192, 442617, 733495, 429111, 778170, 662942, 683141, 868047, 795172, 847543,
        30533,  278793, 29210,  808805, 633385, 732750, 201617, 219813, 214139, 93971,  549100,
        789940, 6788,   914866, 905837, 1223,   904988, 412100, 134501, 225125,
    };

    std::vector<float> const data_untouched(data);
    std::optional<std::ofstream> none(std::nullopt);
    data = codec::reverse_dct(codec::dct(std::move(data), none));

    for (std::size_t index = 0; index < data.size(); index += 1)
        REQUIRE(data[index] - data_untouched[index] < 500);
}

TEST_CASE("zig-zag", "[codec][zigzag]") {
    {
        std::vector<int> image{0,  1,  2,  3,  //
                               4,  5,  6,  7,  //
                               8,  9,  10, 11, //
                               12, 13, 14, 15, //
                               16, 17, 18, 19, //
                               20, 21, 22, 23, //
                               24, 25, 26, 27, //
                               28, 29, 30, 31};
        std::vector<int> const zigzagged{0,  1,  4,  8,  5,  2,  3,  6,  9,  12, 13,
                                         10, 7,  11, 14, 15, 16, 17, 20, 24, 21, 18,
                                         19, 22, 25, 28, 29, 26, 23, 27, 30, 31};

        CHECK(codec::zigzag(std::move(image)) == zigzagged);
    }

    {
        std::vector<int> image{80, 37, 49, 9, 81, 81, 56, 86, 76, 55, 82, 63, 89, 45, 32, 72};
        std::vector<int> const zigzagged{
            80, 37, 81, 76, 81, 49, 9, 56, 55, 89, 45, 82, 86, 63, 32, 72};

        CHECK(codec::zigzag(std::move(image)) == zigzagged);
    }
}

TEST_CASE("quantisation", "[codec][quantisation]") {
    std::optional<std::ofstream> none(std::nullopt);
    CHECK(
        codec::quantise(
            std::vector<float>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
            std::vector<int>{1, 2},
            none) == std::vector<int>{0, 1, 2, 2, 4, 3, 6, 4, 8, 5, 10, 6, 12, 7, 14, 8});
    CHECK(
        codec::quantise(
            std::vector<float>{5, 9, 7, 6, 3, 4, 5, 2, 3, 10, 56, 47},
            std::vector<int>{1, 1000, 1000, 1},
            none) == std::vector<int>{5, 0, 0, 6, 3, 0, 0, 2, 3, 0, 0, 47});
}

TEST_CASE("dequantisation", "[codec][dequantisation]") {
    {
        std::vector<float> result(codec::dequantise(
            std::vector<int>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
            std::vector<int>{1, 10}));

        std::vector<int> result_int(result.begin(), result.end());

        CHECK(
            result_int ==
            std::vector<int>{0, 10, 2, 30, 4, 50, 6, 70, 8, 90, 10, 110, 12, 130, 14, 150});
    }

    {
        std::vector<float> result(codec::dequantise(
            std::vector<int>{15, 64, 255, 98, 63, 25, 47, 89, 12, 47, 58, 69},
            std::vector<int>{1, 2, 2, 1}));

        std::vector<int> result_int(result.begin(), result.end());

        CHECK(result_int == std::vector<int>{15, 128, 510, 98, 63, 50, 94, 89, 12, 94, 116, 69});
    }
}

TEST_CASE("reverse-zig-zag", "[codec][unzigzag]") {
    {
        std::vector<int> const image{0,  1,  2,  3,  //
                                     4,  5,  6,  7,  //
                                     8,  9,  10, 11, //
                                     12, 13, 14, 15, //
                                     16, 17, 18, 19, //
                                     20, 21, 22, 23, //
                                     24, 25, 26, 27, //
                                     28, 29, 30, 31};
        std::vector<int> zigzagged{0,  1,  4,  8,  5,  2,  3,  6,  9,  12, 13, 10, 7,  11, 14, 15,
                                   16, 17, 20, 24, 21, 18, 19, 22, 25, 28, 29, 26, 23, 27, 30, 31};

        CHECK(image == codec::undo_zigzag(std::move(zigzagged)));
    }

    {
        std::vector<int> const image{80, 37, 49, 9, 81, 81, 56, 86, 76, 55, 82, 63, 89, 45, 32, 72};
        std::vector<int> zigzagged{80, 37, 81, 76, 81, 49, 9, 56, 55, 89, 45, 82, 86, 63, 32, 72};

        CHECK(image == codec::undo_zigzag(std::move(zigzagged)));
    }
}

TEST_CASE("an unzig-zagged zig-zagged image is equal to the original", "[codec][zigzag-unzigzag]") {
    auto image = GENERATE(chunk(16 * 16, take(16 * 16 * 16, random(0, 100))));
    std::vector<int> const image_copy(image);
    CHECK(image_copy == codec::undo_zigzag(codec::zigzag(std::move(image))));
}
