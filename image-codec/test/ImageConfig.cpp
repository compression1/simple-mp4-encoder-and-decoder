#include "codec/ImageConfig.hpp"
#include "codec/Bits.hpp"
#include <catch2/catch.hpp>
#include <memory>
#include <sstream>
#include <vector>

void check_config(codec::ImageConfig const &original) {
    BitsWriter writer(std::make_unique<std::stringstream>());
    original.encode(writer);
    writer.flush();

    std::string data = ((std::stringstream *)(writer.get_stream()))->str();
    BitsReader reader(std::make_unique<std::stringstream>(data));
    codec::ImageConfig const restored = codec::ImageConfig::decode(reader);

    CHECK(original.width == restored.width);
    CHECK(original.height == restored.height);
    CHECK(original.use_rle == restored.use_rle);
    CHECK(original.quantisation_factors == restored.quantisation_factors);
    CHECK(original == restored);
}

TEST_CASE("ImageConfig encoding works", "[codec][ImageConfig]") {
    check_config(codec::ImageConfig({1, 2, 3, 4, 5}, 15, 19, true));
    check_config(codec::ImageConfig({1, 2, 3, 4, 5}, 15, 19, false));
    check_config(codec::ImageConfig(
        {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384},
        123456789,
        987654321,
        false));
}
