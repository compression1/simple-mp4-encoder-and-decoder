#include "codec/Bits.hpp"
#include <catch2/catch.hpp>
#include <climits>
#include <iostream>
#include <memory>
#include <sstream>

void check_bool(std::vector<bool> original) {
    BitsWriter writer(std::make_unique<std::stringstream>());
    for (bool const b : original)
        writer.write_bool(b);
    writer.flush();

    std::string data = ((std::stringstream *)(writer.get_stream()))->str();
    CHECK(data.size() * CHAR_BIT >= original.size());

    BitsReader reader(std::make_unique<std::stringstream>(data));

    std::vector<bool> restored;
    restored.reserve(original.size());
    for (std::size_t index = 0; index < original.size(); index += 1)
        restored.push_back(reader.read_bool());

    CHECK(original == restored);
}

TEST_CASE("encoding bools works", "[codec][Bits]") {
    check_bool({false});
    check_bool({true});
    check_bool(std::vector<bool>(CHAR_BIT * 5, false));
    check_bool(std::vector<bool>(CHAR_BIT * 5, true));
    check_bool(std::vector<bool>(CHAR_BIT + CHAR_BIT / 2, false));
    check_bool(std::vector<bool>(CHAR_BIT + CHAR_BIT / 2, true));
    check_bool({true, false, true, false, true, false, true, false, true, false});
    check_bool({true, false, false, false, false, false, false, false, false, true});
}

void check_uints(std::vector<std::pair<std::size_t, int>> original) {
    BitsWriter writer(std::make_unique<std::stringstream>());
    for (auto const [bits, value] : original)
        writer.write_uint32(value, bits);
    writer.flush();

    std::string data = ((std::stringstream *)(writer.get_stream()))->str();
    CHECK(data.size() * CHAR_BIT >= original.size());

    BitsReader reader(std::make_unique<std::stringstream>(data));

    std::vector<std::pair<std::size_t, int>> restored;
    restored.reserve(original.size());
    for (auto const [bits, value] : original)
        restored.push_back(std::make_pair(bits, reader.read_uint32(bits)));

    // for (std::size_t index = 0; index < original.size(); index += 1)
    //     std::cerr << original[index].second << " ";
    // std::cerr << std::endl;
    // for (std::size_t index = 0; index < original.size(); index += 1)
    //     std::cerr << restored[index].second << " ";
    // std::cerr << std::endl;
    // std::cerr << std::endl;

    CHECK(original == restored);
}

void check_ints(std::vector<std::pair<std::size_t, int>> original) {
    BitsWriter writer(std::make_unique<std::stringstream>());
    for (auto const [bits, value] : original)
        writer.write_int32(value, bits);
    writer.flush();

    std::string data = ((std::stringstream *)(writer.get_stream()))->str();
    CHECK(data.size() * CHAR_BIT >= original.size());

    BitsReader reader(std::make_unique<std::stringstream>(data));

    std::vector<std::pair<std::size_t, int>> restored;
    restored.reserve(original.size());
    for (auto const [bits, value] : original)
        restored.push_back(std::make_pair(bits, reader.read_int32(bits)));

    // for (std::size_t index = 0; index < original.size(); index += 1)
    //     std::cerr << original[index].second << " ";
    // std::cerr << std::endl;
    // for (std::size_t index = 0; index < original.size(); index += 1)
    //     std::cerr << restored[index].second << " ";
    // std::cerr << std::endl;
    // std::cerr << std::endl;

    CHECK(original == restored);
}

TEST_CASE("encoding unsigned numbers works", "[codec][Bits][unsigned]") {
    check_uints({
        {8, 0},
        {8, 1},
        {8, 2},
        {8, 3},
        {8, 4},
        {8, 5},
        {8, 6},
        {8, 7},
        {8, 8},
        {8, 9},
        {8, 10},
        {8, 11},
        {8, 12},
        {8, 13},
        {8, 14},
        {8, 15},
    });

    check_uints({{32, 4294967295}});
}

TEST_CASE("encoding signed numbers works", "[codec][Bits]") {
    check_ints({
        {4, -8},
        {4, -7},
        {4, -6},
        {4, -5}, //
        {4, -4},
        {4, -3},
        {4, -2},
        {4, -1}, //
        {4, 0},
        {4, 1},
        {4, 2},
        {4, 3}, //
        {4, 4},
        {4, 5},
        {4, 6},
        {4, 7} //
    });

    check_ints({
        {16, 9951},   {16, -7459},  {16, 6735},   {16, -19445}, {16, -242},  //
        {16, -26588}, {16, -27184}, {16, -25949}, {16, -15441}, {16, -7023}, //
        {16, 29581},  {16, 9355},   {16, 28306},  {16, 1515},   {16, 4612},  //
        {16, 13038},  {16, 5310},   {16, 19557},  {16, 24452},  {16, 20479}  //
    });

    check_ints({{3, -4}, {3, -3}, {3, -2}, {3, -1}, {3, 0}, {3, 1}, {3, 2}, {3, 3}});
    check_ints({{32, -2147483648}, {32, 2147483647}});
}

TEST_CASE("encoding ascii characters works", "[codec][Bits][ascii]") {
    check_ints(
        {{8, -128}, {8, -127}, {8, -126}, {8, -125}, {8, -124}, {8, -123}, {8, -122}, {8, -121},
         {8, -120}, {8, -119}, {8, -118}, {8, -117}, {8, -116}, {8, -115}, {8, -114}, {8, -113},
         {8, -112}, {8, -111}, {8, -110}, {8, -109}, {8, -108}, {8, -107}, {8, -106}, {8, -105},
         {8, -104}, {8, -103}, {8, -102}, {8, -101}, {8, -100}, {8, -99},  {8, -98},  {8, -97},
         {8, -96},  {8, -95},  {8, -94},  {8, -93},  {8, -92},  {8, -91},  {8, -90},  {8, -89},
         {8, -88},  {8, -87},  {8, -86},  {8, -85},  {8, -84},  {8, -83},  {8, -82},  {8, -81},
         {8, -80},  {8, -79},  {8, -78},  {8, -77},  {8, -76},  {8, -75},  {8, -74},  {8, -73},
         {8, -72},  {8, -71},  {8, -70},  {8, -69},  {8, -68},  {8, -67},  {8, -66},  {8, -65},
         {8, -64},  {8, -63},  {8, -62},  {8, -61},  {8, -60},  {8, -59},  {8, -58},  {8, -57},
         {8, -56},  {8, -55},  {8, -54},  {8, -53},  {8, -52},  {8, -51},  {8, -50},  {8, -49},
         {8, -48},  {8, -47},  {8, -46},  {8, -45},  {8, -44},  {8, -43},  {8, -42},  {8, -41},
         {8, -40},  {8, -39},  {8, -38},  {8, -37},  {8, -36},  {8, -35},  {8, -34},  {8, -33},
         {8, -32},  {8, -31},  {8, -30},  {8, -29},  {8, -28},  {8, -27},  {8, -26},  {8, -25},
         {8, -24},  {8, -23},  {8, -22},  {8, -21},  {8, -20},  {8, -19},  {8, -18},  {8, -17},
         {8, -16},  {8, -15},  {8, -14},  {8, -13},  {8, -12},  {8, -11},  {8, -10},  {8, -9},
         {8, -8},   {8, -7},   {8, -6},   {8, -5},   {8, -4},   {8, -3},   {8, -2},   {8, -1},
         {8, 0},    {8, 1},    {8, 2},    {8, 3},    {8, 4},    {8, 5},    {8, 6},    {8, 7},
         {8, 8},    {8, 9},    {8, 10},   {8, 11},   {8, 12},   {8, 13},   {8, 14},   {8, 15},
         {8, 16},   {8, 17},   {8, 18},   {8, 19},   {8, 20},   {8, 21},   {8, 22},   {8, 23},
         {8, 24},   {8, 25},   {8, 26},   {8, 27},   {8, 28},   {8, 29},   {8, 30},   {8, 31},
         {8, 32},   {8, 33},   {8, 34},   {8, 35},   {8, 36},   {8, 37},   {8, 38},   {8, 39},
         {8, 40},   {8, 41},   {8, 42},   {8, 43},   {8, 44},   {8, 45},   {8, 46},   {8, 47},
         {8, 48},   {8, 49},   {8, 50},   {8, 51},   {8, 52},   {8, 53},   {8, 54},   {8, 55},
         {8, 56},   {8, 57},   {8, 58},   {8, 59},   {8, 60},   {8, 61},   {8, 62},   {8, 63},
         {8, 64},   {8, 65},   {8, 66},   {8, 67},   {8, 68},   {8, 69},   {8, 70},   {8, 71},
         {8, 72},   {8, 73},   {8, 74},   {8, 75},   {8, 76},   {8, 77},   {8, 78},   {8, 79},
         {8, 80},   {8, 81},   {8, 82},   {8, 83},   {8, 84},   {8, 85},   {8, 86},   {8, 87},
         {8, 88},   {8, 89},   {8, 90},   {8, 91},   {8, 92},   {8, 93},   {8, 94},   {8, 95},
         {8, 96},   {8, 97},   {8, 98},   {8, 99},   {8, 100},  {8, 101},  {8, 102},  {8, 103},
         {8, 104},  {8, 105},  {8, 106},  {8, 107},  {8, 108},  {8, 109},  {8, 110},  {8, 111},
         {8, 112},  {8, 113},  {8, 114},  {8, 115},  {8, 116},  {8, 117},  {8, 118},  {8, 119},
         {8, 120},  {8, 121},  {8, 122},  {8, 123},  {8, 124},  {8, 125},  {8, 126},  {8, 127}});
}
