#include "codec/rle.hpp"
#include <algorithm>
#include <cassert>

std::size_t const MAX_PADDING = 4;
std::size_t const BLOCK_SIZE = 1 << MAX_PADDING;

codec::RlePoint::RlePoint(std::uint8_t padding, int value) : padding(padding), value(value) {}

void codec::RlePoint::encode(BitsWriter &writer, std::size_t const bits_for_value) const {
    writer.write_uint32(this->padding, 4);
    writer.write_int32(this->value, bits_for_value);
}

codec::RlePoint codec::RlePoint::decode(BitsReader &reader, std::size_t const bits_for_value) {
    std::uint8_t padding = reader.read_uint32(4);
    int value = reader.read_int32(bits_for_value);

    return RlePoint(padding, value);
}

void codec::runlength_encode(std::vector<int> const &data, BitsWriter &writer) {
    assert(data.size() % BLOCK_SIZE == 0);

    std::size_t const block_count = data.size() / BLOCK_SIZE;
    std::size_t padding = 0;

    std::vector<int> rearranged;
    rearranged.reserve(data.size());

    // magic
    for (std::size_t component_index = 0; component_index < BLOCK_SIZE; component_index += 1)
        for (auto iter = data.begin() + component_index; iter < data.end(); iter += BLOCK_SIZE)
            rearranged.push_back(*iter);

    for (std::size_t block_index = 0; block_index < block_count; block_index += 1) {
        auto const [min_value, max_value] = std::minmax_element(
            rearranged.begin() + (block_index * BLOCK_SIZE),
            rearranged.begin() + ((block_index + 1) * BLOCK_SIZE));

        std::size_t const bits_required = std::max(
            {std::size_t(1), bits_required_signed(*min_value), bits_required_signed(*max_value)});

        writer.write_uint32(bits_required - 1, 4);

        for (std::size_t index = 0; index < BLOCK_SIZE; index += 1) {
            int const value = rearranged[block_index * BLOCK_SIZE + index];

            if (value != 0) {
                RlePoint(padding, value).encode(writer, bits_required);
                padding = 0;
            } else
                padding += 1;
        }

        if (padding != 0) {
            RlePoint(0, 0).encode(writer, bits_required);
            padding = 0;
        }
    }
}

void add_padding(std::vector<int> &data, std::size_t const amount) {
    for (std::size_t index = 0; index < amount; index += 1)
        data.push_back(0);
}

std::vector<int> codec::runlength_decode(BitsReader &reader, std::size_t const amount) {
    std::vector<int> data;
    data.reserve(amount);

    std::size_t data_size = 0;
    std::size_t bits_required = 0;

    while (data_size < amount) {
        if (data_size % BLOCK_SIZE == 0)
            bits_required = reader.read_uint32(4) + 1;

        codec::RlePoint const point = codec::RlePoint::decode(reader, bits_required);

        if (point.value == 0) {
            std::size_t const to_pad = BLOCK_SIZE - (data_size % BLOCK_SIZE);
            data_size += to_pad;
            add_padding(data, to_pad);
        } else {
            data_size += point.padding + 1;
            add_padding(data, point.padding);
            data.push_back(point.value);
        }
    }

    std::vector<int> rearranged;
    rearranged.reserve(data.size());

    // magic
    std::size_t offset = data.size() / BLOCK_SIZE;
    for (std::size_t component_index = 0; component_index < offset; component_index += 1)
        for (auto iter = data.begin() + component_index; iter < data.end(); iter += offset)
            rearranged.push_back(*iter);

    return rearranged;
}
