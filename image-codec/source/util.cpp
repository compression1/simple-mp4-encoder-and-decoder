#include "codec/util.hpp"
#include <fstream>
#include <functional>

namespace codec {
    int const BYTES_TAKEN_BY_HUFFMAN_TABLES_SIZE = 2;
    int const TABLE_MARKER_SIZE = 1;
    int const AMT_HUFFMAN_CODES_SIZE = 16;
    int const VALUE_SIZE = 2;
    int const DATA_SIZE = 4;
    int const AMT_ZEROS_BITS_SIZE = 4;
    constexpr int const COEFFICIENT_BITS_SIZE = 8 * sizeof(coefficient_t);

    std::vector<int> matrix_from_file(std::filesystem::path const &filepath) {
        std::ifstream file(filepath);

        std::string line;
        std::string cell;
        std::vector<int> matrix_raw;

        while (!file.eof()) {
            int value;
            file >> value;
            if (!file.fail() || file.bad())
                matrix_raw.push_back(value);
        }

        if ((file.fail() || file.bad()) && !file.eof())
            throw std::runtime_error("error reading matrix file");

        return matrix_raw;
    }

    std::vector<char> read_file(std::filesystem::path const &path) {
        std::ifstream file(path, std::ios::binary | std::ios::in);
        file.exceptions(std::ios::failbit | std::ios::badbit);

        return std::vector<char>(
            std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());
    }

    std::vector<char> const vec_uint8_into_vec_char(std::vector<std::uint8_t> &&data) {
        std::vector<char> result;
        result.reserve(data.size() * sizeof(char));
        for (uint8_t const byte : data)
            result.push_back((unsigned char)(std::move(byte)));
        return result;
    }
} // namespace codec

std::vector<char> codec::ints_to_chars(std::vector<int> const &input) {
    std::vector<char> result;
    result.reserve(input.size());

    constexpr int const min = std::numeric_limits<char>::min();
    constexpr int const max = std::numeric_limits<char>::max();

    for (int const element : input)
        result.push_back(char(std::clamp(std::move(element), min, max)));

    return result;
}

std::vector<int> codec::chars_to_ints(std::vector<char> const &input) {
    std::vector<int> image;
    image.reserve(input.size());
    image.insert(image.begin(), input.begin(), input.end());
    return image;
}
