#include "codec/codec.hpp"
#include "codec/dct.hpp"
#include "codec/quantisation.hpp"
#include "codec/rle.hpp"
#include "codec/util.hpp"
#include <algorithm>
#include <fstream>
#include <iterator>
#include <limits>
#include <sstream>
#include <string>

void codec::encode(
    BitsWriter &writer,
    std::vector<char> const &raw_image,
    codec::ImageConfig const &config,
    std::optional<std::ofstream> &logfile) {

    config.encode(writer);

    std::vector<int> image;
    image.reserve(raw_image.size());
    for (char elem : raw_image)
        image.push_back((unsigned char)(elem));

    codec::encode_image(writer, image, config, logfile);
}

void codec::encode_image(
    BitsWriter &writer,
    std::vector<char> const &image,
    ImageConfig const &config,
    std::optional<std::ofstream> &logfile) {

    return encode_image(writer, codec::chars_to_ints(image), config, logfile);
}

void codec::encode_image(
    BitsWriter &writer,
    std::vector<int> const &image,
    ImageConfig const &config,
    std::optional<std::ofstream> &logfile) {

    encode_dct_image(
        writer,
        forward_dct(image, config.quantisation_factors, config.width, logfile),
        config.use_rle);
}

std::vector<int> codec::decode_image(BitsReader &reader, ImageConfig const &config) {
    return reverse_dct(decode_dct_image(reader, config), config.quantisation_factors, config.width);
}

std::vector<char> const codec::decode(BitsReader &reader) {

    ImageConfig const config = ImageConfig::decode(reader);
    auto const frame = codec::decode_image(reader, config);
    return codec::ints_to_chars(frame);
}

std::vector<int> codec::forward_dct(
    std::vector<int> const &image,
    std::vector<int> const &quantisation_factors,
    std::size_t const width,
    std::optional<std::ofstream> &logfile) {

    std::vector<int> blocks(image_to_blocks(image, BLOCK_SIZE, BLOCK_SIZE, width));
    return dct_and_quantise(std::move(blocks), quantisation_factors, logfile);
}

std::vector<int> codec::reverse_dct(
    std::vector<int> &&image,
    std::vector<int> const &quantisation_factors,
    std::size_t const width) {

    image = undo_dct_and_quantise(std::move(image), quantisation_factors);
    image = blocks_to_image<int>(std::move(image), BLOCK_SIZE, BLOCK_SIZE, width);
    return image;
}

void codec::encode_dct_image(BitsWriter &writer, std::vector<int> &&image, bool const use_rle) {
    image = zigzag(std::move(image));

    if (use_rle)
        codec::runlength_encode(image, writer);
    else
        for (int value : image)
            writer.write_int32(value, 16);
}

std::vector<int> codec::decode_dct_image(BitsReader &reader, codec::ImageConfig const &config) {
    std::size_t image_size = config.width * config.height;
    std::vector<int> image;

    if (config.use_rle)
        image = codec::runlength_decode(reader, image_size);
    else
        for (std::size_t index = 0; index < image_size; index += 1)
            image.push_back(reader.read_int32(16));

    return undo_zigzag(std::move(image));
}

namespace codec {

    /**
     * Splits a line on whitespaces.
     */
    std::vector<std::string> const split_line(std::string const &line) {
        std::istringstream line_stream(line);
        std::vector<std::string> split_line(
            (std::istream_iterator<std::string>(line_stream)),
            std::istream_iterator<std::string>());
        return split_line;
    }

    std::vector<int> dct_and_quantise(
        std::vector<int> &&image,
        std::vector<int> const &factors,
        std::optional<std::ofstream> &logfile) {
        return quantise(
            dct(std::vector<float>(
                    std::make_move_iterator(image.begin()), std::make_move_iterator(image.end())),
                logfile),
            factors,
            logfile);
    }

    std::vector<int>
    undo_dct_and_quantise(std::vector<int> &&image, std::vector<int> const &factors) {
        std::vector<float> result(reverse_dct(dequantise(image, factors)));
        return std::vector<int>(
            std::make_move_iterator(result.begin()), std::make_move_iterator(result.end()));
    }
} // namespace codec
