#include "codec/dct.hpp"
#include "codec/codec.hpp"
#include "codec/util.hpp"
#include <Eigen/Core>
#include <fstream>

Eigen::Matrix<float, codec::BLOCK_SIZE, codec::BLOCK_SIZE> const A = [] {
    Eigen::Matrix<float, codec::BLOCK_SIZE, codec::BLOCK_SIZE> a;
    a << 0.500, +0.500, +0.500, +0.500, //
        +0.653, +0.271, -0.271, -0.653, //
        +0.500, -0.500, -0.500, +0.500, //
        +0.271, -0.653, +0.653, -0.271;
    return a;
}();

Eigen::Matrix<float, codec::BLOCK_SIZE, codec::BLOCK_SIZE> const At = A.transpose();

std::vector<float> codec::dct(std::vector<float> &&image, std::optional<std::ofstream> &logfile) {
    for (std::size_t index = 0; index < image.size(); index += MATRIX_SIZE) {
        Eigen::Matrix<float, BLOCK_SIZE, BLOCK_SIZE, Eigen::RowMajor> matrix =
            Eigen::Map<Eigen::Matrix<float, BLOCK_SIZE, BLOCK_SIZE, Eigen::RowMajor> const>(
                &image.at(index));
        matrix = A * matrix * At;
        auto const data = matrix.data();

        for (std::size_t data_index = 0; data_index < MATRIX_SIZE; data_index += 1)
            image.at(index + data_index) = data[data_index];
    }

    if (logfile) {
        logfile.value() << std::string("DCT EXAMPLES:") << std::endl;
        for (std::size_t i = 0; i < 4; i++) {
            codec::log_block(image, logfile.value(), i);
        }
    }

    return image;
}

std::vector<float> codec::reverse_dct(std::vector<float> &&image) {
    for (std::size_t index = 0; index < image.size(); index += MATRIX_SIZE) {
        Eigen::Matrix<float, BLOCK_SIZE, BLOCK_SIZE, Eigen::RowMajor> matrix =
            Eigen::Map<Eigen::Matrix<float, BLOCK_SIZE, BLOCK_SIZE, Eigen::RowMajor> const>(
                &image.at(index));
        matrix = At * matrix * A;
        auto const data = matrix.data();

        for (std::size_t data_index = 0; data_index < MATRIX_SIZE; data_index += 1)
            image.at(index + data_index) = data[data_index];
    }

    return image;
}
