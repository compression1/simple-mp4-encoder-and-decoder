#include "codec/ImageConfig.hpp"

codec::ImageConfig::ImageConfig(
    std::vector<int> &&quantisation_factors,
    std::size_t const width,
    std::size_t const height,
    bool use_rle)
    : quantisation_factors(quantisation_factors), width(width), height(height), use_rle(use_rle) {}

void codec::ImageConfig::encode(BitsWriter &writer) const {
    writer.write_uint32(this->quantisation_factors.size(), 32);
    for (int factor : this->quantisation_factors)
        writer.write_int32(factor, 16);

    writer.write_uint32(this->width, 32);
    writer.write_uint32(this->height, 32);
    writer.write_bool(this->use_rle);
}

codec::ImageConfig codec::ImageConfig::decode(BitsReader &reader) {
    std::size_t const factor_count = reader.read_uint32(32);
    std::vector<int> quantisation_factors;
    quantisation_factors.reserve(factor_count);

    for (std::size_t index = 0; index < factor_count; index += 1)
        quantisation_factors.push_back(reader.read_int32(16));

    std::size_t const width = reader.read_uint32(32);
    std::size_t const height = reader.read_uint32(32);
    bool use_rle = reader.read_bool();

    return ImageConfig(std::move(quantisation_factors), width, height, use_rle);
}

bool codec::ImageConfig::operator==(ImageConfig const &that) const {
    return this->use_rle == that.use_rle && this->width == that.width &&
           this->height == that.height && this->quantisation_factors == that.quantisation_factors;
}
