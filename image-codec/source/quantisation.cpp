#include "codec/quantisation.hpp"
#include "codec/util.hpp"
#include <cmath>
#include <fstream>

std::vector<int> codec::quantise(
    std::vector<float> const &image,
    std::vector<int> const &factors,
    std::optional<std::ofstream> &logfile) {
    std::vector<int> result;
    result.reserve(image.size());

    for (std::size_t index = 0; index < image.size(); index += 1) {
        int const factor = factors[index % factors.size()];
        result.push_back(lround(image[index] / float(factor)));
    }

    if (logfile) {
        logfile.value() << std::string("QUANTISATION EXAMPLES:") << std::endl;
        for (std::size_t i = 0; i < 4; i++) {
            codec::log_block(image, logfile.value(), i);
        }
    }

    return result;
}

std::vector<float>
codec::dequantise(std::vector<int> const &image, std::vector<int> const &factors) {
    std::vector<float> result;
    result.reserve(image.size());

    for (std::size_t index = 0; index < image.size(); index += 1) {
        int const factor = factors[index % factors.size()];
        result.push_back(image[index] * factor);
    }

    return result;
}
