#include "codec/Bits.hpp"
#include <climits>
#include <cmath>
#include <fstream>
#include <sstream>

BitsWriter::BitsWriter(std::unique_ptr<std::ostream> &&stream)
    : stream(std::move(stream)), buffer(0), buffer_position(0) {
    this->stream->exceptions(std::ios::failbit | std::ios::badbit);
}

BitsWriter::BitsWriter(std::filesystem::path const &file)
    : stream(std::make_unique<std::ofstream>(file, std::ios::out | std::ios::binary)),
      buffer(0),
      buffer_position(0) {}

BitsWriter::~BitsWriter() { this->flush(); }

void BitsWriter::write_bool(bool const bit) {
    if (bit)
        this->buffer |= 1 << this->buffer_position;

    this->buffer_position += 1;

    if (buffer_position == CHAR_BIT)
        this->flush();
}

void BitsWriter::write_uint32(std::uint32_t value, std::size_t bit_count) {
    for (std::size_t bit_index = 0; bit_index < bit_count; bit_index += 1)
        this->write_bool((value & 1 << bit_index) != 0);
}

void BitsWriter::write_int32(std::int32_t value, std::size_t bit_count) {
    std::uint32_t adjusted_value = std::uint64_t(value) + (1 << (bit_count - 1));
    this->write_uint32(adjusted_value, bit_count);
}

void BitsWriter::flush() {
    if (this->buffer_position != 0) {
        this->stream->write(&this->buffer, 1);
        this->buffer = 0;
        this->buffer_position = 0;
    }
}

std::ostream const *BitsWriter::get_stream() const { return this->stream.get(); }

BitsReader::BitsReader(std::unique_ptr<std::istream> &&stream)
    : stream(std::move(stream)), buffer(0), buffer_position(CHAR_BIT) {
    this->stream->exceptions(std::ios::failbit | std::ios::badbit);
}

BitsReader::BitsReader(std::filesystem::path const &file)
    : BitsReader(std::make_unique<std::ifstream>(file, std::ios::in | std::ios::binary)) {}

bool BitsReader::read_bool() {
    if (this->buffer_position == CHAR_BIT) {
        this->stream->read(&this->buffer, 1);
        this->buffer_position = 0;
    }

    return (this->buffer & (1 << (this->buffer_position++))) != 0;
}

std::uint32_t BitsReader::read_uint32(std::size_t const bit_count) {
    std::uint32_t value = 0;

    for (std::size_t bit_index = 0; bit_index < bit_count; bit_index += 1)
        value |= this->read_bool() ? (1 << bit_index) : 0;

    return value;
}

std::int32_t BitsReader::read_int32(std::size_t const bit_count) {
    std::uint32_t value = this->read_uint32(bit_count);
    return std::int64_t(value) - (1 << (bit_count - 1));
}

bool BitsReader::data_left() const {
    return this->buffer_position != CHAR_BIT || !this->stream->eof();
}

std::size_t bits_required_unsigned(std::uint32_t value) {
    std::size_t bits_required = 0;

    while (value) {
        value >>= 1;
        bits_required += 1;
    }

    return bits_required;
}

std::size_t bits_required_signed(std::int32_t value) {
    return bits_required_unsigned(std::size_t(std::abs(std::int64_t(value)))) + 1;
}
