#include "app.hpp"
#include "CLI/CLI.hpp"
#include "codec/ImageConfig.hpp"
#include "codec/util.hpp"
#include "video_codec/VideoConfig.hpp"
#include "video_codec/codec.hpp"
#include "video_codec/yuv.hpp"
#include <filesystem>

struct Options {
    std::filesystem::path rawfile;
    std::filesystem::path encfile;
    std::size_t width;
    std::size_t height;
    bool rle;
    std::filesystem::path quantfile;
    std::optional<std::filesystem::path> logfile;
    std::size_t gop;
    std::size_t merange;

    Options(CLI::App &application, int const arg_count, char const *const args[]) {
        application.set_config("config,-c,--config", "config.ini", "read config from file");

        application.add_option("--rawfile", this->rawfile, "read video data from this file")
            ->required(true)
            ->check(CLI::ExistingFile);
        application
            .add_option(
                "--encfile", this->encfile, "write compressed and encoded video to this file")
            ->required(true);
        application.add_option("--width", this->width, "width of the video")->required(true);
        application.add_option("--height", this->height, "height of the video")->required(true);
        application.add_option("--quantfile", this->quantfile, "load quantisation matrix")
            ->required(true)
            ->check(CLI::ExistingFile);
        application.add_flag("-r,--rle", this->rle, "use runlength encoding")->required(true);
        application.add_option("-l,--logfile", this->logfile, "log debug information to file");
        application.add_option("--gop", this->gop, "gop value for encoding")->required(true);
        application
            .add_option(
                "--merange",
                this->merange,
                "half search window side length in pixels, used during encoding")
            ->required(true);

        application.parse(arg_count, args);
    }
};

void encode(Options const &options) {
    std::optional<std::ofstream> logfile = std::nullopt;

    if (options.logfile) {
        std::filesystem::create_directories(
            std::filesystem::absolute(options.logfile.value()).parent_path());
        logfile = std::make_optional<std::ofstream>(options.logfile.value(), std::ios::out);
    }

    video_codec::VideoConfig config(
        codec::ImageConfig(
            codec::matrix_from_file(options.quantfile), options.width, options.height, options.rle),
        options.gop,
        options.merange);

    if (logfile)
        logfile.value() << "Read " << config.image.quantisation_factors.size()
                        << " quantisation factors." << std::endl;

    video_codec::RawFrameReader reader(options.rawfile, config.image.width, config.image.height);

    if (logfile)
        logfile.value() << "Detected " << reader.remaining_frames() << " frames." << std::endl;

    std::filesystem::create_directories(std::filesystem::absolute(options.encfile).parent_path());
    BitsWriter writer(options.encfile);
    video_codec::encode(writer, reader, config, logfile);
}

int main(int const argc, char const *const argv[]) {
    CLI::App application{"Compress and encode videos."};

    try {
        Options options(application, argc, argv);
        encode(options);
    } catch (CLI::ParseError const &error) {
        return application.exit(error);
    }
}
