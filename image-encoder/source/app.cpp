#include "CLI/CLI.hpp"
#include "codec/codec.hpp"
#include "codec/util.hpp"
#include <filesystem>
#include <iostream>
#include <optional>
#include <stdexcept>

struct Options {
    std::filesystem::path rawfile;
    std::filesystem::path encfile;
    std::filesystem::path quantfile;
    std::optional<std::filesystem::path> logfile;
    std::size_t width;
    std::size_t height;
    bool rle;

    Options(CLI::App &application, int const arg_count, char const *const args[]) {
        application.set_config("config,-c,--config", "config.ini", "read config from file");
        application.add_option("--rawfile", this->rawfile, "read raw image from this file")
            ->required(true)
            ->check(CLI::ExistingFile);
        application
            .add_option(
                "--encfile", this->encfile, "save compressed and encoded image to this file")
            ->required(true);
        application.add_option("--quantfile", this->quantfile, "load quantisation matrix")
            ->required(true)
            ->check(CLI::ExistingFile);
        application.add_option("-l,--logfile", this->logfile, "log debug information to file");
        application.add_option("--width", this->width, "width of the image")->required(true);
        application.add_option("--height", this->height, "height of the image")->required(true);
        application.add_flag("-r,--rle", this->rle, "use runlength encoding")->default_val("false");

        // unused but needed by decoder thus should be parseable
        std::filesystem::path _;
        application.add_option("--decfile", _, "")->required(false);

        application.parse(arg_count, args);
    }
};

void encode(Options const &options) {
    std::optional<std::ofstream> logfile = std::nullopt;

    if (options.logfile) {
        std::filesystem::create_directories(
            std::filesystem::absolute(options.logfile.value()).parent_path());
        logfile = std::make_optional<std::ofstream>(options.logfile.value(), std::ios::out);
    }

    std::vector<char> const image(codec::read_file(options.rawfile));
    auto const image_size = image.size();
    std::size_t const width = options.width;
    std::size_t const height = image.size() / options.width;

    if (logfile)
        logfile.value() << "Read image with size of " << image_size << " bytes." << std::endl;

    codec::ImageConfig config(
        codec::matrix_from_file(options.quantfile), width, height, options.rle);

    std::filesystem::create_directories(std::filesystem::absolute(options.encfile).parent_path());
    BitsWriter writer(options.encfile);
    codec::encode(writer, std::move(image), config, logfile);
}

int main(int const argc, char const *const argv[]) {
    CLI::App application{"Compress and encode images"};

    try {
        Options options(application, argc, argv);
        encode(options);
    } catch (CLI::ParseError const &error) {
        return application.exit(error);
    }
}
