#include "app.hpp"
#include "CLI/CLI.hpp"
#include "codec/util.hpp"
#include "video_codec/codec.hpp"
#include <filesystem>

struct Options {
    std::filesystem::path encfile;
    std::filesystem::path decfile;
    std::optional<std::filesystem::path> logfile;
    bool motion_compensation;

    Options(CLI::App &application, int const arg_count, char const *const args[]) {
        application.set_config("config,-c,--config", "config.ini", "read config from file");

        application
            .add_option("--encfile", this->encfile, "read data to be decompressed from this file")
            ->required(true)
            ->check(CLI::ExistingFile);
        application.add_option("--decfile", this->decfile, "write playable video to this file")
            ->required(true);
        application
            .add_flag(
                "--motioncompensation",
                this->motion_compensation,
                "use motioncompensation. Without this the motion vectors should be somewhat "
                "visible in the output video")
            ->required(true);
        application.add_option("-l,--logfile", this->logfile, "log debug information to file");

        application.parse(arg_count, args);
    }
};

void decode(Options const &options) {
    std::optional<std::ofstream> logfile = std::nullopt;

    if (options.logfile) {
        std::filesystem::create_directories(
            std::filesystem::absolute(options.logfile.value()).parent_path());
        logfile = std::make_optional<std::ofstream>(options.logfile.value(), std::ios::out);
    }

    BitsReader reader(options.encfile);
    video_codec::RawFrameWriter writer(options.decfile);

    video_codec::decode(reader, writer, options.motion_compensation, logfile);
}

int main(int const argc, char const *const argv[]) {
    CLI::App application{"Compress and encode videos."};

    try {
        Options options(application, argc, argv);
        decode(options);
    } catch (CLI::ParseError const &error) {
        return application.exit(error);
    }
}
