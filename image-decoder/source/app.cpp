#include "app.hpp"
#include "CLI/CLI.hpp"
#include "codec/codec.hpp"
#include "codec/util.hpp"
#include <filesystem>

struct Options {
    std::filesystem::path encfile;
    std::filesystem::path decfile;
    std::filesystem::path quantfile;
    std::optional<std::filesystem::path> logfile;
    std::size_t width;
    std::size_t height;
    bool rle;

    Options(CLI::App &application, int const arg_count, char const *const args[]) {
        application.set_config("config,-c,--config", "config.ini", "read config from file");
        application
            .add_option(
                "--encfile", this->encfile, "read compressed and encoded image from this file")
            ->required(true)
            ->check(CLI::ExistingFile);
        application
            .add_option(
                "--decfile", this->decfile, "save compressed and decoded image to this file")
            ->required(true);
        application.add_option("--quantfile", this->quantfile, "load quantisation matrix")
            ->check(CLI::ExistingFile);
        application.add_option("-l,--logfile", this->logfile, "log debug information to file");
        application.add_option("--width", this->width, "width of the image");
        application.add_option("--height", this->height, "height of the image");
        application.add_flag("-r,--rle", this->rle, "use runlength encoding");

        // unused but needed by encoder thus should be parseable
        std::filesystem::path _;
        application.add_option("--rawfile", _, "")->required(false);

        application.parse(arg_count, args);
    }
};

void decode(Options const &options) {
    std::optional<std::ofstream> logfile = std::nullopt;

    if (options.logfile) {
        std::filesystem::create_directories(
            std::filesystem::absolute(options.logfile.value()).parent_path());
        logfile = std::make_optional<std::ofstream>(options.logfile.value(), std::ios::out);
    }

    BitsReader reader(options.encfile);
    auto const decoded(codec::decode(reader));

    std::filesystem::create_directories(std::filesystem::absolute(options.decfile).parent_path());
    std::ofstream file(options.decfile, std::ios::out | std::ios::binary);
    file.exceptions(std::ios::failbit | std::ios::badbit);

    file.write(decoded.data(), decoded.size());
}

// TODO: logging
int main(int const argc, char const *const argv[]) {
    CLI::App application{"Compress and decode images"};

    try {
        Options options(application, argc, argv);
        decode(options);
    } catch (CLI::ParseError const &error) {
        return application.exit(error);
    }
}
