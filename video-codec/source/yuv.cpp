#include "video_codec/yuv.hpp"
#include <fstream>
#include <iterator>

video_codec::RawFrameReader::RawFrameReader(
    std::unique_ptr<std::istream> &&stream,
    std::size_t const frame_size,
    std::size_t const frame_padding,
    std::size_t const frame_count)
    : stream(std::move(stream)),
      frame_size(frame_size),
      frame_padding(frame_padding),
      frame_count(frame_count) {
    this->stream->exceptions(std::ios::failbit | std::ios::badbit);
}

video_codec::RawFrameReader::RawFrameReader(
    std::filesystem::path const &path,
    std::size_t const frame_width,
    std::size_t const frame_height)
    : RawFrameReader(
          std::make_unique<std::ifstream>(path, std::ios::in | std::ios::binary),
          frame_width * frame_height,
          frame_width * frame_height / 2,
          std::filesystem::file_size(path) / (frame_width * frame_height / 2 * 3)) {}

bool video_codec::RawFrameReader::read_frame(std::vector<char> &to) {
    if (this->frame_count > 0) {
        to.resize(this->frame_size);
        this->stream->read(to.data(), this->frame_size);
        for (char &elem : to)
            elem = (char)(int((unsigned char)(elem)) - 128);
        this->stream->ignore(this->frame_padding);
        this->frame_count -= 1;
        return true;
    } else
        return false;
}

std::size_t video_codec::RawFrameReader::remaining_frames() const { return this->frame_count; }

video_codec::RawFrameWriter::RawFrameWriter(std::unique_ptr<std::ostream> &&stream)
    : stream(std::move(stream)) {
    this->stream->exceptions(std::ios::failbit | std::ios::badbit);
}

video_codec::RawFrameWriter::RawFrameWriter(std::filesystem::path const &path)
    : RawFrameWriter(std::make_unique<std::ofstream>(path, std::ios::out | std::ios::binary)) {}

std::vector<char> video_codec::RawFrameWriter::write_frame(std::vector<char> &&frame) {
    auto const frame_in_schar_cp = frame;

    for (char &elem : frame)
        elem = (unsigned char)((int)(elem) + 128);
    this->stream->write(frame.data(), frame.size());
    std::fill_n(std::ostream_iterator<char>(*this->stream), frame.size() / 2, 128);

    return frame_in_schar_cp;
}
