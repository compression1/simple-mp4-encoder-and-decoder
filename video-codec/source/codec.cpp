#include "video_codec/codec.hpp"
#include "codec/util.hpp"
#include "video_codec/util.hpp"
#include <algorithm>
#include <fstream>

using std::make_move_iterator;

std::size_t const MOTIONV_GROUP_SIZE = 2;

std::vector<frame_idx_t>
video_codec::diff_encode_motion_vecs(std::vector<frame_idx_t> const &&motion_vecs) {
    auto last_motion_vec = 0;
    std::vector<frame_idx_t> diff_encoded_motion_vecs{};

    for (auto const &motion_vec : motion_vecs) {
        diff_encoded_motion_vecs.push_back(motion_vec - last_motion_vec);
        last_motion_vec = motion_vec;
    }

    return diff_encoded_motion_vecs;
}

std::vector<frame_idx_t>
video_codec::diff_decode_motion_vecs(std::vector<frame_idx_t> const &&motion_vecs) {
    auto last_motion_vec = 0;
    std::vector<frame_idx_t> diff_decoded_motion_vecs{};

    for (auto const &motion_vec : motion_vecs) {
        diff_decoded_motion_vecs.push_back(motion_vec + last_motion_vec);
        last_motion_vec += motion_vec;
    }

    return diff_decoded_motion_vecs;
}

void video_codec::write_motion_vecs(
    BitsWriter &writer,
    std::vector<frame_idx_t> const &motion_vecs) {

    auto const amt_motion_vecs = motion_vecs.size();

    for (size_t block_idx = 0; block_idx < amt_motion_vecs; block_idx += MOTIONV_GROUP_SIZE) {
        auto const [min_value, max_value] = std::minmax_element(
            motion_vecs.begin() + block_idx,
            motion_vecs.begin() + std::min(block_idx + MOTIONV_GROUP_SIZE, amt_motion_vecs));

        std::size_t const bits_required =
            std::max({bits_required_signed(*min_value), bits_required_signed(*max_value)});

        writer.write_uint32(bits_required, 5);

        std::vector<frame_idx_t> const wtf_vec{
            motion_vecs.begin() + block_idx,
            motion_vecs.begin() + std::min(block_idx + MOTIONV_GROUP_SIZE, amt_motion_vecs)};
        auto const end =
            motion_vecs.begin() + std::min(block_idx + MOTIONV_GROUP_SIZE, amt_motion_vecs);
        for (auto iter = motion_vecs.begin() + block_idx; iter < end; iter += 1) {
            writer.write_int32(*iter, bits_required);
        }
    }
}

std::vector<frame_idx_t>
video_codec::read_motion_vecs(BitsReader &reader, codec::ImageConfig const &config) {

    std::vector<frame_idx_t> motion_vecs{};
    auto const amt_motion_vecs = config.width * config.height / video_codec::MACRO_BLOCK_AMT_PIXELS;

    for (size_t block_idx = 0; block_idx < amt_motion_vecs; block_idx += MOTIONV_GROUP_SIZE) {

        std::size_t const bits_required = reader.read_uint32(5);

        for (size_t i = block_idx; i < std::min(amt_motion_vecs, block_idx + MOTIONV_GROUP_SIZE);
             i += 1)
            motion_vecs.push_back(reader.read_int32(bits_required));
    }

    return motion_vecs;
}

std::vector<char> video_codec::encode_iframe(
    BitsWriter &writer,
    std::vector<char> const &frame,
    codec::ImageConfig const &config) {

    std::optional<std::ofstream> log;

    std::vector<int> dct_image = codec::forward_dct(
        codec::chars_to_ints(frame), config.quantisation_factors, config.width, log);
    codec::encode_dct_image(writer, std::vector<int>(dct_image), config.use_rle);

    return codec::ints_to_chars(
        codec::reverse_dct(std::move(dct_image), config.quantisation_factors, config.width));
}

std::vector<char> video_codec::decode_iframe(BitsReader &reader, codec::ImageConfig const &config) {
    auto const frame = codec::decode_image(reader, config);
    return codec::ints_to_chars(frame);
}

std::vector<char> video_codec::encode_pframe(
    BitsWriter &writer,
    std::vector<char> const &target_frame,
    std::vector<char> const &reference_frame,
    codec::ImageConfig const &config,
    std::size_t merange) {
    // Yes, this is cheating, but we use propper debuggers :)
    std::optional<std::ofstream> log;

    auto [motion_vecs, block_diffs] = video_codec::decompose_p_frame(
        codec::chars_to_ints(reference_frame),
        codec::chars_to_ints(target_frame),
        config.width,
        merange);

    auto const diff_encoded_motion_vecs =
        video_codec::diff_encode_motion_vecs(std::vector<frame_idx_t>(motion_vecs));
    video_codec::write_motion_vecs(writer, std::move(diff_encoded_motion_vecs));

    // Decoder can only work on compressed data. If the next frame would thus be a pframe, this
    // would have to be based on this compressed frame. On the decoder this frame itself consists of
    // 2 compressed parts: reference frame + compressed block differences. To allow the encoder to
    // base itself on a compressed frame we thus have to build this one here and return it. We
    // compress the block diffs and rebuild this frame based on the compressed reference frame.
    auto dct_block_diffs =
        codec::forward_dct(block_diffs, config.quantisation_factors, config.width, log);
    codec::encode_dct_image(writer, std::vector<int>(dct_block_diffs), config.use_rle);
    auto const compressed_block_diffs =
        codec::reverse_dct(std::move(dct_block_diffs), config.quantisation_factors, config.width);

    return codec::ints_to_chars(video_codec::compose_p_frame(
        codec::chars_to_ints(reference_frame), motion_vecs, compressed_block_diffs, config.width));
}

std::vector<char> video_codec::decode_pframe(
    BitsReader &reader,
    std::vector<char> const &reference_frame,
    codec::ImageConfig const &config,
    bool const motion_compensation) {

    auto const diff_encoded_motion_vecs = video_codec::read_motion_vecs(reader, config);
    auto const block_diffs = codec::decode_image(reader, config);

    auto const motion_vecs =
        video_codec::diff_decode_motion_vecs(std::move(diff_encoded_motion_vecs));
    return codec::ints_to_chars(video_codec::compose_p_frame(
        codec::chars_to_ints(reference_frame),
        motion_vecs,
        block_diffs,
        config.width,
        motion_compensation));
}

void video_codec::encode(
    BitsWriter &writer,
    video_codec::RawFrameReader &reader,
    video_codec::VideoConfig const &config,
    std::optional<std::ofstream> &logfile) {

    config.encode(writer);

    std::size_t const frame_size = config.image.width * config.image.height;
    std::vector<char> original_frame, lossy_frame;
    original_frame.resize(frame_size);
    lossy_frame.resize(frame_size);

    std::size_t frame_index = 0;

    while (reader.read_frame(original_frame)) {
        std::cout << frame_index << std::endl;
        // indicate another frame existing
        writer.write_bool(true);

        if (frame_index++ % config.gop == 0)
            lossy_frame = video_codec::encode_iframe(writer, original_frame, config.image);
        else
            lossy_frame = video_codec::encode_pframe(
                writer, original_frame, lossy_frame, config.image, config.merange);
    }

    // No more frames
    writer.write_bool(false);
}

void video_codec::decode(
    BitsReader &reader,
    RawFrameWriter &writer,
    bool const motion_compensation,
    std::optional<std::ofstream> &logfile) {

    VideoConfig config = VideoConfig::decode(reader);
    std::size_t const frame_size = config.image.width * config.image.height;
    std::size_t const full_frame_size = config.image.width * config.image.height / 2 * 3;
    std::size_t const uv_count = full_frame_size - frame_size;

    std::vector<char> yuv_video;
    std::vector<char> curr_frame(frame_size);
    std::vector<char> prev_frame(frame_size);
    std::size_t frame_index = 0;

    while (reader.read_bool()) {
        std::cout << frame_index << std::endl;
        prev_frame.swap(curr_frame);

        if (frame_index++ % config.gop == 0)
            curr_frame = decode_iframe(reader, config.image);
        else
            curr_frame = decode_pframe(reader, prev_frame, config.image, motion_compensation);

        curr_frame = writer.write_frame(std::move(curr_frame));
    }
}
