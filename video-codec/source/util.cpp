#include "video_codec/util.hpp"
#include "codec/util.hpp"
#include "video_codec/block_matching.hpp"

using std::make_move_iterator;

// There isn't really a pixel in the 'middle' of a block of 16x16. We take 1 of the middle 4
// pixels.
frame_idx_t video_codec::block_middle_from_topleft(
    frame_idx_t const block_topleft_idx,
    frame_idx_t const frame_width) {
    return block_topleft_idx + video_codec::MACRO_BLOCK_SIDE_LEN / 2 +
           video_codec::MACRO_BLOCK_SIDE_LEN / 2 * frame_width;
}

frame_idx_t video_codec::block_topleft_from_middle(
    frame_idx_t const block_middle_idx,
    frame_idx_t const frame_width) {
    return block_middle_idx - video_codec::MACRO_BLOCK_SIDE_LEN / 2 -
           video_codec::MACRO_BLOCK_SIDE_LEN / 2 * frame_width;
}

std::vector<int> video_codec::copy_block_from_frame(
    std::vector<int> const &frame,
    frame_idx_t const block_topleft_idx,
    size_t const frame_width) {
    std::vector<int> block{};

    for (frame_idx_t l = 0; l < video_codec::MACRO_BLOCK_SIDE_LEN; l += 1) {
        for (frame_idx_t k = 0; k < video_codec::MACRO_BLOCK_SIDE_LEN; k += 1) {
            auto const pixel_idx = block_topleft_idx + l * (frame_idx_t)frame_width + k;

            // out of frame check
            auto const pixel =
                (pixel_idx >= 0 && pixel_idx < frame.size()) ? frame.at(pixel_idx) : 0;
            block.push_back(pixel);
        }
    }

    return block;
}

std::vector<int> video_codec::insert_block_into_frame(
    std::vector<int> &&frame,
    std::vector<int> const &block,
    frame_idx_t const block_topleft_idx,
    size_t const frame_width) {
    for (frame_idx_t l = 0; l < video_codec::MACRO_BLOCK_SIDE_LEN; l += 1) {
        frame_idx_t const cache = l * video_codec::MACRO_BLOCK_SIDE_LEN;
        frame_idx_t const cache2 = block_topleft_idx + l * (frame_idx_t)frame_width;
        for (frame_idx_t k = 0; k < video_codec::MACRO_BLOCK_SIDE_LEN; k += 1) {
            auto const block_item_idx = cache + k;
            auto const pixel_idx = cache2 + k;
            frame[pixel_idx] = block[block_item_idx];
        }
    }

    return std::move(frame);
}

std::vector<int>
video_codec::sum_blocks(std::vector<int> const &&lhs, std::vector<int> const &&rhs) {
    if (lhs.size() != rhs.size()) {
        throw std::invalid_argument("Cannot sum blocks with different sizes.");
    }

    std::vector<int> result{};
    for (size_t i = 0; i < lhs.size(); i += 1) {
        result.push_back(lhs[i] + rhs[i]);
    }

    return result;
}

std::vector<int>
video_codec::diff_blocks(std::vector<int> const &&lhs, std::vector<int> const &&rhs) {
    if (lhs.size() != rhs.size()) {
        throw std::invalid_argument("Cannot subtract blocks with different sizes.");
    }

    std::vector<int> result{};
    for (size_t i = 0; i < lhs.size(); i += 1) {
        result.push_back(lhs[i] - rhs[i]);
    }

    return result;
}

bool video_codec::block_is_inside_frame(
    frame_idx_t const block_topleft_idx,
    size_t const frame_width,
    size_t const amt_pixels) {
    auto const block_bottomright_idx =
        block_topleft_idx + (frame_idx_t)(frame_width * (video_codec::MACRO_BLOCK_SIDE_LEN - 1)) +
        (frame_idx_t)(video_codec::MACRO_BLOCK_SIDE_LEN - 1);
    return block_topleft_idx >= 0 && block_bottomright_idx < amt_pixels;
}

bool video_codec::block_is_inside_search_block(
    frame_idx_t const block_topleft_idx,
    frame_idx_t const search_block_middle_idx,
    size_t const merange,
    size_t const frame_width) {
    auto const block_bottomright_idx =
        block_topleft_idx + (frame_idx_t)(frame_width * (video_codec::MACRO_BLOCK_SIDE_LEN - 1)) +
        (frame_idx_t)(video_codec::MACRO_BLOCK_SIDE_LEN - 1);
    auto const search_block_topleft_idx =
        search_block_middle_idx - (frame_idx_t)(frame_width * merange) - (frame_idx_t)merange;
    auto const search_block_bottomright_idx =
        search_block_middle_idx + (frame_idx_t)(frame_width * merange) + (frame_idx_t)merange;

    return block_topleft_idx >= search_block_topleft_idx &&
           block_bottomright_idx <= search_block_bottomright_idx;
}
