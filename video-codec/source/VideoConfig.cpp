#include "video_codec/VideoConfig.hpp"

video_codec::VideoConfig::VideoConfig(
    codec::ImageConfig &&image,
    std::size_t const gop,
    std::size_t const merange = 0)
    : image(image), gop(gop), merange(merange) {}

void video_codec::VideoConfig::encode(BitsWriter &writer) const {
    this->image.encode(writer);
    writer.write_uint32(this->gop, 32);
}

video_codec::VideoConfig video_codec::VideoConfig::decode(BitsReader &reader) {
    codec::ImageConfig image = codec::ImageConfig::decode(reader);
    std::size_t const gop = reader.read_uint32(32);

    return video_codec::VideoConfig(std::move(image), gop);
}
