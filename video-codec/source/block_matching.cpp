#include "video_codec/block_matching.hpp"
#include "video_codec/util.hpp"
#include <algorithm>
#include <cmath>

using video_codec::frame_idx_t;

std::vector<int> video_codec::compose_p_frame(
    std::vector<int> const &ref_frame,
    std::vector<frame_idx_t> const &motion_vecs,
    std::vector<int> const &block_diffs,
    size_t const frame_width,
    bool const motion_compensation) {
    // We assume that frames are nicely divisible by the macro block size, just as in the image
    // coding assignment.
    if (motion_vecs.size() != ref_frame.size() / video_codec::MACRO_BLOCK_AMT_PIXELS) {
        throw std::invalid_argument("Amount of macro blocks to be extracted from encoded format "
                                    "does not match amount of blocks in the reference frame.");
    }

    std::vector<int> rebuilt_frame(ref_frame.size());

    auto const amt_blocks_in_row = frame_width / video_codec::MACRO_BLOCK_SIDE_LEN;
    auto const amt_blocks_in_col =
        ref_frame.size() / frame_width / video_codec::MACRO_BLOCK_SIDE_LEN;

    for (size_t target_block_row_idx = 0; target_block_row_idx < amt_blocks_in_col;
         target_block_row_idx += 1) {
        for (size_t target_block_col_idx = 0; target_block_col_idx < amt_blocks_in_row;
             target_block_col_idx += 1) {

            auto const target_block_topleft_idx =
                target_block_row_idx * amt_blocks_in_row * video_codec::MACRO_BLOCK_AMT_PIXELS +
                target_block_col_idx * video_codec::MACRO_BLOCK_SIDE_LEN;
            // Motion vec was stored as the diff to the target block's topleft
            auto const ref_block_topleft_idx =
                (frame_idx_t)target_block_topleft_idx -
                motion_vecs.at(target_block_row_idx * amt_blocks_in_row + target_block_col_idx);

            {
                auto const ref_block = video_codec::copy_block_from_frame(
                    ref_frame, ref_block_topleft_idx, frame_width);

                std::vector<int> block_diff{};
                if (motion_compensation) {
                    block_diff = video_codec::copy_block_from_frame(
                        block_diffs, target_block_topleft_idx, frame_width);
                } else {
                    block_diff.assign(video_codec::MACRO_BLOCK_AMT_PIXELS, 0);
                }

                auto const resulting_target_block =
                    video_codec::sum_blocks(std::move(block_diff), std::move(ref_block));

                rebuilt_frame = video_codec::insert_block_into_frame(
                    std::move(rebuilt_frame),
                    resulting_target_block,
                    target_block_topleft_idx,
                    frame_width);
            }
        }
    }

    return rebuilt_frame;
}

std::tuple<std::vector<frame_idx_t>, std::vector<int>> video_codec::decompose_p_frame(
    std::vector<int> const &ref_frame,
    std::vector<int> const &target_frame,
    size_t const frame_width,
    size_t const merange) {

    std::vector<frame_idx_t> motion_vecs;
    motion_vecs.reserve(ref_frame.size() / video_codec::MACRO_BLOCK_AMT_PIXELS);

    std::vector<int> block_diffs(ref_frame.size());

    auto const amt_blocks_in_row = frame_width / video_codec::MACRO_BLOCK_SIDE_LEN;
    auto const amt_blocks_in_col =
        ref_frame.size() / frame_width / video_codec::MACRO_BLOCK_SIDE_LEN;

    for (size_t target_block_row_idx = 0; target_block_row_idx < amt_blocks_in_col;
         target_block_row_idx += 1) {
        for (size_t target_block_col_idx = 0; target_block_col_idx < amt_blocks_in_row;
             target_block_col_idx += 1) {

            auto const target_block_topleft_idx =
                target_block_row_idx * amt_blocks_in_row * video_codec::MACRO_BLOCK_AMT_PIXELS +
                target_block_col_idx * video_codec::MACRO_BLOCK_SIDE_LEN;

            auto const ref_block_topleft_idx = video_codec::find_motion_vec_2d_log(
                ref_frame,
                target_frame,
                target_block_topleft_idx,
                target_block_topleft_idx,
                frame_width,
                merange,
                merange,
                target_frame.size());

            {
                auto const ref_block = video_codec::copy_block_from_frame(
                    ref_frame, ref_block_topleft_idx, frame_width);
                auto const target_block = video_codec::copy_block_from_frame(
                    target_frame, target_block_topleft_idx, frame_width);
                auto const block_diff =
                    video_codec::diff_blocks(std::move(target_block), std::move(ref_block));

                // The block diffs are stored with the same layout as frames, i.e.
                // forming an 'image'. This way they can be nicely compressed with DCT, etc.
                block_diffs = video_codec::insert_block_into_frame(
                    std::move(block_diffs), block_diff, target_block_topleft_idx, frame_width);
                // Store motion vec as diff to target block's topleft
                motion_vecs.push_back(target_block_topleft_idx - ref_block_topleft_idx);
            }
        }
    }

    return {motion_vecs, block_diffs};
}

frame_idx_t video_codec::find_motion_vec_2d_log(
    std::vector<int> const &ref_frame,
    std::vector<int> const &target_frame,
    frame_idx_t const ref_block_topleft_idx,
    frame_idx_t const target_block_topleft_idx,
    size_t const frame_width,
    size_t const merange,
    size_t const orig_merange,
    size_t const amt_pixels) {
    // The recursive block's merange cannot be made even smaller.
    if (merange == 1) {
        return ref_block_topleft_idx;
    }

    frame_idx_t const uniform_offset = ceil((float)merange / 2.0);
    frame_idx_t motion_vec = 0;
    float min_mad = std::numeric_limits<float>::max();
    auto const potential_ref_block_middle_idx =
        video_codec::block_middle_from_topleft(ref_block_topleft_idx, frame_width);
    auto const search_block_middle_idx =
        video_codec::block_middle_from_topleft(target_block_topleft_idx, frame_width);

    for (frame_idx_t y = -uniform_offset; y < uniform_offset + 1; y += uniform_offset) {
        auto const y_offset = y * (frame_idx_t)frame_width;
        for (frame_idx_t x = -uniform_offset; x < uniform_offset + 1; x += uniform_offset) {
            auto const x_offset = x;

            auto const potential_ref_block_topleft = video_codec::block_topleft_from_middle(
                potential_ref_block_middle_idx + x_offset + y_offset, frame_width);
            auto const ref_block_inside_frame = video_codec::block_is_inside_frame(
                potential_ref_block_topleft, frame_width, amt_pixels);
            auto const ref_block_inside_search_block = video_codec::block_is_inside_search_block(
                potential_ref_block_topleft, search_block_middle_idx, orig_merange, frame_width);

            if (ref_block_inside_frame && ref_block_inside_search_block) {
                auto const potential_mad = video_codec::calc_mad(
                    ref_frame,
                    target_frame,
                    potential_ref_block_topleft,
                    target_block_topleft_idx,
                    frame_width,
                    amt_pixels);
                if (potential_mad < min_mad) {
                    motion_vec = potential_ref_block_topleft;
                    min_mad = potential_mad;
                }
            }
        }
    }

    return video_codec::find_motion_vec_2d_log(
        ref_frame,
        target_frame,
        motion_vec,
        target_block_topleft_idx,
        frame_width,
        uniform_offset,
        orig_merange,
        amt_pixels);
}

float video_codec::calc_mad(
    std::vector<int> const &ref_frame,
    std::vector<int> const &target_frame,
    frame_idx_t const ref_block_topleft_idx,
    frame_idx_t const target_block_topleft_idx,
    size_t const frame_width,
    size_t const amt_pixels) {
    auto sum_diff = 0.0;
    auto counted_pixels = 0;

    for (size_t l = 0; l < video_codec::MACRO_BLOCK_SIDE_LEN; l += 1) {
        auto const l_offset = (frame_idx_t)(l * frame_width);
        for (size_t k = 0; k < video_codec::MACRO_BLOCK_SIDE_LEN; k += 1) {
            auto const k_offset = (frame_idx_t)k;
            auto const target_idx = target_block_topleft_idx + l_offset + k_offset;
            auto const ref_idx = ref_block_topleft_idx + l_offset + k_offset;

            // When out of bounds we simply don't count the difference.
            // `target_idx` is not checked as it should never go out of bounds (if it does it
            // should thus not be ignored). It is namely based on the top left index of a block
            // and only incremented till it reaches the bottom right of a block.
            if (ref_idx >= 0 && ref_idx < amt_pixels) {
                sum_diff += abs(target_frame[target_idx] - ref_frame[ref_idx]);
                counted_pixels += 1;
            }
        }
    }

    return sum_diff / (float)counted_pixels;
}
