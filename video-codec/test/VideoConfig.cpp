#include "video_codec/VideoConfig.hpp"
#include "codec/Bits.hpp"
#include <catch2/catch.hpp>
#include <memory>
#include <sstream>
#include <vector>

void check_config(video_codec::VideoConfig const &original) {
    BitsWriter writer(std::make_unique<std::stringstream>());
    original.encode(writer);
    writer.flush();

    std::string data = ((std::stringstream *)(writer.get_stream()))->str();
    BitsReader reader(std::make_unique<std::stringstream>(data));
    video_codec::VideoConfig const restored = video_codec::VideoConfig::decode(reader);

    CHECK(original.image == restored.image);
    CHECK(original.gop == restored.gop);
}

TEST_CASE("VideoConfig encoding works", "[codec][VideoConfig]") {
    check_config(video_codec::VideoConfig(codec::ImageConfig({1, 2, 3, 4, 5}, 15, 19, true), 1, 1));
    check_config(
        video_codec::VideoConfig(codec::ImageConfig({1, 2, 3, 4, 5}, 15, 19, true), 512, 10));
    check_config(video_codec::VideoConfig(
        codec::ImageConfig({1, 2, 3, 4, 5}, 15, 19, true), 1010101010, 30));
}
