#pragma once

#ifndef VIDEOCONFIG_H
#define VIDEOCONFIG_H

#include "codec/Bits.hpp"
#include "codec/ImageConfig.hpp"
#include <vector>

namespace video_codec {

    /**
     * Settings used for video compression.
     */
    struct VideoConfig {
        codec::ImageConfig image;
        std::size_t const gop;
        std::size_t const merange;

        VideoConfig(codec::ImageConfig &&image, std::size_t gop, std::size_t merange);

        void encode(BitsWriter &writer) const;
        static VideoConfig decode(BitsReader &reader);
    };
} // namespace video_codec

#endif // VIDEOCONFIG_H
