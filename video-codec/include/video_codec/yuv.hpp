#pragma once

#ifndef YUV_HPP
#define YUV_HPP

#include <filesystem>
#include <istream>
#include <memory>
#include <ostream>
#include <vector>

namespace video_codec {
    class RawFrameReader {
        std::unique_ptr<std::istream> stream;
        std::size_t frame_size;
        std::size_t frame_padding;
        std::size_t frame_count;

      public:
        RawFrameReader(
            std::unique_ptr<std::istream> &&stream,
            std::size_t frame_size,
            std::size_t frame_padding,
            std::size_t frame_count);

        RawFrameReader(
            std::filesystem::path const &path,
            std::size_t frame_width,
            std::size_t frame_height);

        bool read_frame(std::vector<char> &to);

        std::size_t remaining_frames() const;
    };

    class RawFrameWriter {
        std::unique_ptr<std::ostream> stream;

      public:
        RawFrameWriter(std::unique_ptr<std::ostream> &&stream);
        RawFrameWriter(std::filesystem::path const &path);

        std::vector<char> write_frame(std::vector<char> &&frame);
    };
} // namespace video_codec

#endif // YUV_HPP
