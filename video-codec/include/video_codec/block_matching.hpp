#pragma once

#include <cstdint>
#include <tuple>
#include <vector>

namespace video_codec {
    // We use int to index a frame as matching blocks/motion vectors may (partially) go out of
    // bounds of a frame.
    typedef int frame_idx_t;

    int const MACRO_BLOCK_SIDE_LEN = 16;
    int const MACRO_BLOCK_AMT_PIXELS = MACRO_BLOCK_SIDE_LEN * MACRO_BLOCK_SIDE_LEN;

    std::vector<int> compose_p_frame(
        std::vector<int> const &ref_frame,
        std::vector<frame_idx_t> const &motion_vecs,
        std::vector<int> const &block_diffs,
        size_t const frame_width,
        bool const motion_compensation = true);

    // Find, per target macro block, a motion vector and calculate the difference between
    // the matching ref block.
    // Motion vectors are stored as the difference between the matching reference block's topleft
    // and the target block's topleft.
    std::tuple<std::vector<frame_idx_t>, std::vector<int>> decompose_p_frame(
        std::vector<int> const &ref_frame,
        std::vector<int> const &target_frame,
        size_t const frame_width,
        size_t const merange);

    frame_idx_t find_motion_vec_2d_log(
        std::vector<int> const &ref_frame,
        std::vector<int> const &target_frame,
        frame_idx_t const ref_block_topleft_idx,
        frame_idx_t const target_block_topleft_idx,
        size_t const frame_width,
        size_t const merange,
        size_t const orig_merange,
        size_t const amt_pixels);

    float calc_mad(
        std::vector<int> const &ref_frame,
        std::vector<int> const &target_frame,
        frame_idx_t const ref_block_topleft_idx,
        frame_idx_t const target_block_topleft_idx,
        size_t const frame_width,
        size_t const amt_pixels);

    std::vector<int> block_sum(
        std::vector<int> const &ref_frame,
        std::vector<int> const &&block_diff,
        std::vector<int> &&target_frame,
        frame_idx_t const ref_block_topleft_idx,
        frame_idx_t const target_block_topleft_idx,
        size_t const frame_width,
        size_t const amt_pixels,
        bool const motion_compensation);

    std::vector<int> block_diff(
        std::vector<int> const &ref_frame,
        std::vector<int> const &target_frame,
        frame_idx_t const ref_block_topleft_idx,
        frame_idx_t const target_block_topleft_idx,
        size_t const frame_width,
        size_t const amt_pixels);
} // namespace video_codec