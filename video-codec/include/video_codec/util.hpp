#pragma once

#include "codec/codec.hpp"
#include "video_codec/block_matching.hpp"
#include <cstdint>
#include <vector>

using video_codec::frame_idx_t;

namespace video_codec {
    float const Y_TO_FRAME_SIZE_FACTOR = 3.0 / 2.0;
    int const GOP_SIZE = sizeof(uint8_t);
    int const HEIGHT_SIZE = sizeof(size_t);

    frame_idx_t
    block_topleft_from_middle(frame_idx_t const block_middle_idx, frame_idx_t const frame_width);
    frame_idx_t
    block_middle_from_topleft(frame_idx_t const block_topleft_idx, frame_idx_t const frame_width);

    // Utility to get a block from the 1D frame vector.
    // It is not possible to return references, to either pixels or rows, as referenced pixels might
    // lay outside of the frame. Moving is also not possible as in some use cases the original frame
    // has to be kept intact.
    // For out of frame pixels we return 0.
    std::vector<int> copy_block_from_frame(
        std::vector<int> const &frame,
        frame_idx_t const block_topleft_idx,
        size_t const frame_width);

    std::vector<int> insert_block_into_frame(
        std::vector<int> &&frame,
        std::vector<int> const &block,
        frame_idx_t const block_topleft_idx,
        size_t const frame_width);

    std::vector<int> sum_blocks(std::vector<int> const &&lhs, std::vector<int> const &&rhs);
    std::vector<int> diff_blocks(std::vector<int> const &&lhs, std::vector<int> const &&rhs);

    bool block_is_inside_frame(
        frame_idx_t const block_topleft_idx,
        size_t const frame_width,
        size_t const amt_pixels);
    bool block_is_inside_search_block(
        frame_idx_t const block_topleft_idx,
        frame_idx_t const search_block_middle_idx,
        size_t const merange,
        size_t const frame_width);
} // namespace video_codec
