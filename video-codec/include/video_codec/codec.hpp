#pragma once

#ifndef VIDEOCODEC_H
#define VIDEOCODEC_H

#include "codec/Bits.hpp"
#include "codec/codec.hpp"
#include "video_codec/VideoConfig.hpp"
#include "video_codec/block_matching.hpp"
#include "video_codec/yuv.hpp"
#include <optional>
#include <tuple>
#include <vector>

using video_codec::frame_idx_t;

namespace video_codec {
    std::vector<frame_idx_t> diff_encode_motion_vecs(std::vector<frame_idx_t> const &&motion_vecs);
    std::vector<frame_idx_t> diff_decode_motion_vecs(std::vector<frame_idx_t> const &&motion_vecs);

    void write_motion_vecs(BitsWriter &writer, std::vector<frame_idx_t> const &motion_vecs);
    std::vector<frame_idx_t> read_motion_vecs(BitsReader &reader, codec::ImageConfig const &config);

    void encode(
        BitsWriter &writer,
        video_codec::RawFrameReader &reader,
        VideoConfig const &config,
        std::optional<std::ofstream> &logfile);

    void decode(
        BitsReader &reader,
        RawFrameWriter &writer,
        bool motion_compensation,
        std::optional<std::ofstream> &logfile);

    std::vector<char> encode_iframe(
        BitsWriter &writer,
        std::vector<char> const &frame,
        codec::ImageConfig const &config);

    std::vector<char> decode_iframe(BitsReader &reader, codec::ImageConfig const &config);

    std::vector<char> encode_pframe(
        BitsWriter &writer,
        std::vector<char> const &frame,
        std::vector<char> const &reference_frame,
        codec::ImageConfig const &config,
        std::size_t merange);

    std::vector<char> decode_pframe(
        BitsReader &reader,
        std::vector<char> const &reference_frame,
        codec::ImageConfig const &config,
        bool motion_compensation);
} // namespace video_codec

#endif // VIDEOCODEC_H
